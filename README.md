# BD_UNICAMP

This is a just docker application to use for database study

- Requirements
    - docker
    - docker-compose
    - Dbeaver (Access the SGDB)
<br/>
- How to use?
<br/>
 Just run `docker-compose up` in root of the project that contains the docker-compose.yml file
The image used for SQL Server is 2017

- To enter in the container in file system execute:
`docker exec -it {name_of_container} bash`