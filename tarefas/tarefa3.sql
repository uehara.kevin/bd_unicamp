use st767

CREATE TABLE PRODUTO(
	cod_produto int NOT NULL,
	nome char(50) NOT NULL,
	preco money NOT NULL,
	qtd_estoque int NOT NULL,
	PRIMARY KEY (cod_produto)
)

CREATE TABLE NOTA_FISCAL(
	num_nota int NOT NULL,
	valor_total money NOT NULL,
	PRIMARY KEY (num_nota)
)

CREATE TABLE ITEM_NOTA_FISCAL(
	num_nota int NOT NULL,
	cod_produto int NOT NULL,
	quantidade int NOT NULL,
	FOREIGN KEY (num_nota) REFERENCES NOTA_FISCAL,
	FOREIGN KEY (cod_produto) REFERENCES PRODUTO,
)

CREATE INDEX index_itemNotaFiscal_notafiscal ON ITEM_NOTA_FISCAL(num_nota)
CREATE INDEX index_itemNotaFiscal_prdouto ON ITEM_NOTA_FISCAL(cod_produto)

CREATE TABLE FATURA(
	num_fatura int NOT NULL,
	dt_vencimento datetime NOT NULL,
	dt_pagamento datetime NULL,
	valor money NOT NULL,
	num_nota int NOT NULL,
	PRIMARY KEY (num_fatura),
	FOREIGN KEY (num_nota) REFERENCES NOTA_FISCAL
)

CREATE INDEX index_fatura_notafiscal ON FATURA(num_nota)

--ex1
CREATE VIEW VISAO_PRODUTOS_NAO_VENDIDOS
as
SELECT p.cod_produto, p.nome, p.qtd_estoque FROM Produto p
LEFT JOIN ITEM_NOTA_FISCAL inf
ON p.cod_produto = inf.cod_produto
WHERE inf.cod_produto is NULL

--ex2
CREATE VIEW VISAO_PRODUTO_VENDIDOS
as
SELECT p.cod_produto, p.nome, sum(inf.quantidade) 'Quantidade Total Vendido' FROM Produto p
INNER JOIN ITEM_NOTA_FISCAL inf
ON p.cod_produto = inf.cod_produto
GROUP BY p.nome, p.cod_produto 

--ex3
CREATE VIEW VISAO_NOTAFISCAL_ITEM_PRODUTO
as
SELECT nf.num_nota, nf.valor_total, p.nome, p.preco, inf.quantidade, inf.quantidade * p.preco 'Valor Vendido' FROM NOTA_FISCAL nf 
INNER JOIN ITEM_NOTA_FISCAL inf 
ON inf.num_nota = nf.num_nota 
INNER JOIN PRODUTO p 
ON p.cod_produto = inf.cod_produto

--ex4
CREATE VIEW VISAO_NOTAFISCAL_FATURA_NAO_PAGA
as
SElECT nf.num_nota, nf.valor_total, f.num_fatura, f.dt_vencimento, f.valor from NOTA_FISCAL nf 
INNER JOIN FATURA f 
ON f.num_nota = nf.num_nota 
WHERE f.dt_pagamento is NULL

--ex5
CREATE VIEW VISAO_NOTAFISCAL_FATURA_PAGA
as
SElECT nf.num_nota, f.valor from NOTA_FISCAL nf 
INNER JOIN FATURA f 
ON f.num_nota = nf.num_nota 
WHERE f.dt_pagamento is NOT NULL