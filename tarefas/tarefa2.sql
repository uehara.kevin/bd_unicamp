-- Exercício 2
use st767

CREATE TABLE MOTORISTA (
	codigo int NOT NULL,
	nome char(50) NOT NULL,
	nro_carteira int NOT NULL,
	hora_entrada time,
	hora_saida time,
	PRIMARY KEY(codigo)
)

CREATE TABLE CLIENTE (
	codigo int NOT NULL,
	rg int NOT NULL,
	nome char(50) NOT NULL,
	endereco char(50) NOT NULL
	PRIMARY KEY(codigo)
)

CREATE TABLE VEICULO (
	placa char(10) NOT NULL,
	marca char(50) NOT NULL,
	cor char(15) NOT NULL,
	PRIMARY KEY(placa)
)

CREATE TABLE OCORRENCIA (
	codigo int NOT NULL,
	end_busca char(50) NOT NULL,
	end_entrega char(50) NOT NULL,
	data datetime,
	distancia int NOT NULL,
	preco money NOT NULL,
	pago char(1) NOT NULL,
	cod_motorista int NOT NULL,
	cod_cliente int NOT NULL,
	placa char(10) NOT NULL,
	PRIMARY KEY(codigo),
	FOREIGN KEY(cod_motorista) REFERENCES MOTORISTA,
	FOREIGN KEY(cod_cliente) REFERENCES CLIENTE,
	FOREIGN KEY(placa) REFERENCES VEICULO
)

CREATE INDEX index_ocorrencia_motorista ON OCORRENCIA(cod_motorista)
CREATE INDEX index_ocorrencia_cliente ON OCORRENCIA(cod_cliente)
CREATE INDEX index_ocorrencia_placa ON OCORRENCIA(placa)

INSERT INTO MOTORISTA VALUES (5, 'Kevin Uehara', 123456789, CURRENT_TIMESTAMP , CURRENT_TIMESTAMP)

INSERT INTO CLIENTE VALUES(1, 397870760, 'Pedro Henrique', 'Rua Luis Ferreira Pires')

INSERT INTO VEICULO VALUES('GOZ1732', 'volkswagen', 'cinza')

INSERT INTO OCORRENCIA VALUES(10, 'Rua Barao de Paranapanema', 'Rua teste', CURRENT_TIMESTAMP, 6, 50.30, 'N', 5, 1, 'GOZ1732')

UPDATE OCORRENCIA SET pago='S' WHERE codigo=10

UPDATE MOTORISTA SET hora_saida='18:00' WHERE codigo=5

DELETE FROM OCORRENCIA WHERE data BETWEEN '2020-10-01' AND '2020-10-10' AND pago='S'

DELETE FROM VEICULO WHERE placa='AAA5555'

SELECT * from MOTORISTA WHERE hora_entrada='06:00' AND hora_saida='13:00'

SELECT COUNT(*) FROM OCORRENCIA WHERE pago='S'

SELECT data, AVG(preco) 'Preço Médio' FROM OCORRENCIA GROUP BY data

SELECT c.nome FROM CLIENTE c INNER JOIN OCORRENCIA o ON c.codigo = o.cod_cliente WHERE o.pago='N'

SELECT v.placa, c.nome, o.data, o.distancia FROM OCORRENCIA o 
INNER JOIN CLIENTE c 
ON c.codigo = o.cod_cliente
INNER JOIN VEICULO v
ON v.placa = o.placa
ORDER BY o.data

SELECT c.nome 'Nome Cliente', m.nome 'Nome Motorista', o.data, o.preco FROM OCORRENCIA o 
INNER JOIN CLIENTE c 
ON c.codigo = o.cod_cliente
INNER JOIN MOTORISTA m
ON m.codigo = o.cod_motorista
ORDER BY o.data, c.nome